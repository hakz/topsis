<?php


if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class m_peserta extends CI_Model {

	function ambil() {
		$ambil = $this->db->get('peserta');
		if ($ambil->num_rows() > 0) {
			foreach ($ambil->result() as $data) {
				$hasil[] = $data;
			}
			return $hasil;
		}
	}


	function ambil_kriteria() {
		$ambil = $this->db->get('kriteria');
		if ($ambil->num_rows() > 0) {
			foreach ($ambil->result() as $data) {
				$hasil[] = $data;
			}
			return $hasil;
		}
	}

	function get_hasil(){
		$ambil = $this->db->query("SELECT nilai.idNilai, peserta.Nama, nilai.Nilai, kriteria.NamaKriteria, kriteria.idKriteria FROM peserta JOIN nilai ON nilai.idPeserta = peserta.idPeserta JOIN kriteria ON kriteria.idKriteria = nilai.idKriteria;");

		if ($ambil->num_rows() > 0) {
			foreach ($ambil->result() as $data) {
				$hasil[] = $data;
			}
			return $hasil;
		}
	}

	function ambilbyid($id="") {
		$ambil = $this->db->get_where('peserta', array('idPeserta' => $id));
		return $ambil;
	}



}

?>