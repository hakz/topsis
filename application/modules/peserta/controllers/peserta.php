<?php

class peserta extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url', 'form'));
		$this->load->model('m_peserta');
	}

	function index(){
		$data['data'] = $this->m_peserta->ambil();
		$this->load->view('global/header');
        $this->load->view("index", $data);
        $this->load->view('global/footer');
	}

	function nilai(){
		$data['data'] = $this->m_peserta->get_hasil();
		$this->load->view('global/header');
        $this->load->view("nilai", $data);
        $this->load->view('global/footer');
	}

	function tambah(){
			$d['tipe'] = 'Tambah';
			$d['Nama'] = "";
			$d['id'] = "";
			$this->load->view('global/header');
			$this->load->view('peserta/tambah', $d);
			$this->load->view('global/footer');

	}

	function tambah_nilai(){
			$data['kriteria'] = $this->m_peserta->ambil_kriteria();
			$data['peserta'] = $this->m_peserta->ambil();
			$this->load->view('global/header');
			$this->load->view('peserta/tambah_nilai', $data);
			$this->load->view('global/footer');

	}


	function edit($id=''){
			$ambil = $this->m_peserta->ambilbyid($id)->row();
			$d['Nama'] = $ambil->Nama;
			$d['tipe'] = 'Edit';
			$d['id'] = $id;
			$this->load->view('global/header');
			$this->load->view('peserta/tambah', $d);
			$this->load->view('global/footer');
	}


	function edit_nilai($id=''){
			$data['kriteria'] = $this->m_peserta->ambil_kriteria();
			$data['peserta'] = $this->m_peserta->ambil();
			$this->load->view('global/header');
			$this->load->view('peserta/tambah_nilai', $data);
			$this->load->view('global/footer');
	}

	function simpan_peserta(){
			$this->form_validation->set_rules('nama', 'Nama Peseta', 'alpha|required');
			if ($this->input->post('submit')) {
				if ($this->form_validation->run() == FALSE) {
					$data['Nama'] = $this->input->post('nama');
					$data['id'] = $this->input->post('id');
					$data['tipe'] = $this->input->post('tipe');
					$this->load->view('global/header');
					$this->load->view('peserta/tambah', $data);
					$this->load->view('global/footer');
				}else{
					if( $this->input->post('tipe')== 'Tambah'){
						$data['Nama'] = $this->input->post('nama');
						print_r($data['Nama']);
						//$this->m_penyakit->tambah_gejala();
						//redirect('peserta/tembah_nilai/');
					}else{
						$id = $this->input->post('id');
						print_r($id.'=>'.$data['Nama']);
						//$this->m_penyakit->update_gejala($id_gejala);
						//redirect('penyakit/edit_gejala/'.$id_gejala);
					}
				}
			}
		
	}

	function simpan_nilai(){
			$this->form_validation->set_rules('nama', 'Nama Peserta', 'alpha|required');
			$this->form_validation->set_rules('NamaKriteria', 'Nama Kriteria', 'alpha|required');
			$this->form_validation->set_rules('nilai', 'Nilai', 'numeric|required');
			if ($this->input->post('submit')) {
				if ($this->form_validation->run() == FALSE) {
					$data['kriteria'] = $this->m_peserta->ambil_kriteria();
					$data['peserta'] = $this->m_peserta->ambil();
					$data['Nama'] = $this->input->post('nama');
					$data['NamaKriteria'] = $this->input->post('NamaKriteria');
					$data['nilai'] = $this->input->post('nilai');
					$data['id'] = $this->input->post('id');
					$data['tipe'] = $this->input->post('tipe');
					$this->load->view('global/header');
					$this->load->view('peserta/tambah_nilai', $data);
					$this->load->view('global/footer');
				}else{
					if( $this->input->post('tipe')== 'Tambah'){
						$data['Nama'] = $this->input->post('nama');
						print_r($data['Nama']);
						//$this->m_penyakit->tambah_gejala();
						//redirect('peserta/tembah_nilai/');
					}else{
						$id = $this->input->post('id');
						print_r($id.'=>'.$data['Nama']);
						//$this->m_penyakit->update_gejala($id_gejala);
						//redirect('penyakit/edit_gejala/'.$id_gejala);
					}
				}
			}
		
	}
}
?>