<div id="page-content">
  <div id="wrap">
    <div id="page-heading">
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>">Dashboard</a></li>
        <li><?php echo $tipe; ?> Data Peserta</li>
      </ol>

      <h1><?php echo $tipe; ?> Data Peserta</h1>

    </div>
    <div class="container">

      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-primary">
            <div class="panel-heading">

              <h4>Form <?php echo $tipe; ?> Data Peserta</h4>

            </div>
            <div class="panel-body">
             <?php if (validation_errors()) { ?>
             <div class="alert alert-dismissable alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4>Terjadi Kesalahan!</h4>
              <?php echo validation_errors(); ?>
            </div>
            <?php } ?>
            <?php if ($this->session->flashdata('result_pass')) { ?>
            <div class="alert alert-dismissable alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4>Terjadi Kesalahan!</h4>
              <?php echo $this->session->flashdata('result_pass'); ?>
            </div>
            <?php } ?>
            <?php if ($this->session->flashdata('result_pass_true')) { ?>
            <div class="alert alert-dismissable alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4>Sukses!</h4>
              <?php echo $this->session->flashdata('result_pass_true'); ?>
            </div>
            <?php } ?>
            <br />
            <?php echo form_open('peserta/simpan_peserta', 'class="form-horizontal"'); ?>
            <div class="form-group">
              <label for="mediuminput" class="col-sm-3 control-label">Nama</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" id="mediuminput" placeholder="Nama Lengkap" value="<?php echo $Nama;?>" name="nama"/>
              </div>
            </div>
          </div>
          <input type="hidden" name="tipe" value="<?php echo $tipe; ?>" />
          <input type="hidden" name="id" value="<?php echo $id; ?>" />
          <div class="panel-footer">
            <div class="row">
              <div class="col-sm-6 col-sm-offset-3">
                <div class="btn-toolbar">
                  <button class="btn-primary btn" type="submit" value="submit" name="submit">Submit</button>
                  <a class="btn-default btn" href="<?php echo site_url('peserta'); ?>">Cancel</a>
                </div>
              </div>
            </div>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>

  </div> <!-- row -->
</div> <!-- container -->
</div> <!-- wrap -->
</div> <!-- page-content -->
