<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
* 
*/
class topsis extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url', 'form'));
		$this->load->model('m_topsis');
	}

	public function index($id=0){
		$peserta = $this->m_topsis->get_peserta();
		$komp = $this->m_topsis->get_kompetensi();
		$data['NamaKompetensi'] = $komp[$id]['NamaKompetensi'];
		$data['NextNama'] = $komp[$id+1]['NamaKompetensi'];
		$data['size'] = sizeof($komp);
		if($id != 0){
		$data['PrevNama'] = $komp[$id-1]['NamaKompetensi'];
		}
		$data['id']= $id;
		$data['hasiltopsis'] = $this->m_topsis->ambilhasilbyid($komp[$id]['idKompetensi'])->result_array();
		$this->load->view('global/header');
        $this->load->view('v_topsis', $data);
        $this->load->view('global/footer');
	}

	public function hasil($id=0)
	{		
		$peserta = $this->m_topsis->get_peserta();
		$komp = $this->m_topsis->get_kompetensi();
		$data['NamaKompetensi'] = $komp[$id]['NamaKompetensi'];
		if($id+1 != sizeof($komp)){
		$data['NextNama'] = $komp[$id+1]['NamaKompetensi'];
		}
		$data['size'] = sizeof($komp);
		if($id != 0){
		$data['PrevNama'] = $komp[$id-1]['NamaKompetensi'];
		}
		$data['id']= $id;
		$data['hasiltopsis'] = $this->m_topsis->ambilhasilbyid($komp[$id]['idKompetensi'])->result_array();
		$this->load->view('global/header');
        $this->load->view('v_topsis', $data);
        $this->load->view('global/footer');
	}

	public function tambah_bobot(){
			$data['kriteria'] = $this->m_topsis->ambil_kriteria();
			$data['kompetensi'] = $this->m_topsis->ambil_kompetensi();
			$this->load->view('global/header');
			$this->load->view('topsis/tambah_bobot', $data);
			$this->load->view('global/footer');
	}


	public function prosestopsis(){
        $hasil = $this->m_topsis->get_hasil();
        $hasil_x = $this->m_topsis->get_x();
        $bobot = $this->m_topsis->bobot_kriteria();
        $peserta = $this->m_topsis->get_peserta();
        $this->m_topsis->create_table_normalisasi();

         echo '<pre>';
      	//** NORMALISASI
	        $m = 0;
	        $temp =0;
	        while ($m < sizeof($hasil_x)) {
	         for ($z=0; $z < sizeof($peserta); $z++) { 
	         		$normalisasi = $hasil[$temp]['Nilai']/$hasil_x[$m]['X'];
	         		$this->m_topsis->insert_normalisasi($normalisasi, $peserta[$z]['idPeserta']);
	        		//print_r($hasil[$i]['Nama'].'=>'.$hasil[$i]['Nilai'].'/'.$hasil_x[$m]['X'].'='.$normalisasi);
	        		//echo '<br/>';
	        		$temp++;
	        	}
	        	$m++;
	        }

	       $data_normalisasi = $this->m_topsis->get_normalisasi();
	    	//print_r($bobot);

	   		//input table with normalisation
	   		$tabel=0;
	   		$temp2 = 0;
	   		$this->m_topsis->create_table_normalisasi_terbobot();
	   		for ($i=0; $i < sizeof($bobot); $i++) { 
	   			for ($j=0; $j < sizeof($peserta); $j++) { 
	   				if($temp2 >= sizeof($data_normalisasi)){
	   					$tabel++;
	   					$temp2 = 0;
	   					//print_r("kriteria lain");
	   					//echo "<br/>";
	   				}
	   				$normalisasi_terbobot = $data_normalisasi[$temp2]['nilai_normalisasi']*$bobot[$i]['Bobot'];
	   				//print_r($j.' '.$normalisasi_terbobot.' '.$bobot[$i]['idKompetensi'].' '.$bobot[$i]['idKriteria']);
	   				//echo "<br/>";
	   				$this->m_topsis->insert_table_normalisasi_terbobot($normalisasi_terbobot, $peserta[$j]['idPeserta'], $bobot[$i]['idKompetensi'], $bobot[$i]['idKriteria']);
	   				//print_r($data_normalisasi[$temp2]['nilai_normalisasi'].'x'.$bobot[$i]['Bobot'].'='.$normalisasi_terbobot);
	   				//		echo "<br/>";
	   				$temp2++;
	   			}
	   		}


	   		//define min and max
	   		$this->m_topsis->create_table_min();
	   		$kriteria = $this->m_topsis->count_kriteria();
	   		$kompetensi = $this->m_topsis->count_kompetensi();
	   		//print_r($kriteria);
	   		for ($l=1; $l <= $kriteria->kriteria; $l++) { 
	   			$mx = $this->m_topsis->min_max($l);
	   			for ($i=0; $i < sizeof($mx); $i++) { 
	   				$this->m_topsis->insert_table_min($mx[$i]['min'],$mx[$i]['max'],$mx[$i]['idKriteria'],$mx[$i]['idKompetensi']);
	   			}
	   		}


	   		//rearange array
	   		$komp = $this->m_topsis->get_kompetensi();
	   		$mn  = array();
	   		$mx  = array();
	   		for ($i=0; $i < sizeof($komp); $i++) { 
	   			$mxid = $this->m_topsis->min_max_normal_where($komp[$i]['idKompetensi']);
	   			for ($j=0; $j < sizeof($mxid); $j++) { 
	   				$mn[] = array('pn' => $mxid[$j]['pn'], 'idKompetensi' => $mxid[$j]['idKompetensi'], 'idPeserta' => $mxid[$j]['idPeserta']);
	   				$mx[] = array('px' => $mxid[$j]['px'], 'idKompetensi' => $mxid[$j]['idKompetensi'], 'idPeserta' => $mxid[$j]['idPeserta']);
	   			}

	   			
	   		}

	   		//print_r($mxid);
	   	
	   		//$mxn = $this->m_topsis->min_max_normalisasi();

	   		//print_r(sizeof($mxn));

	   		$this->m_topsis->create_table_hasiltopsis();
  			$m = 0;
	        $temp =0;
	        $mtn=0; $mtp=0;
	        $idk=0;
	        $idp=0;
	        while ($m < sizeof($peserta)) {
	        	for ($i=0; $i < $kompetensi->kompetensi; $i++) { 
	        		for ($z=0; $z < $kriteria->kriteria; $z++) { 
		         		$mtn = $mn[$temp]['pn']+$mtn;
		         		$mtp = $mx[$temp]['px']+$mtp;
		         		$idk = $mx[$temp]['idKompetensi'];
		         		$idp = $mx[$temp]['idPeserta'];
		         		// print_r($mn[$temp]['pn']);
		         		// echo "<br/>";
		        		$temp++;
	        		}
	        		// print_r('--------------------------+');
	        		// echo "<br/>";
	        		$V = sqrt($mtn)/(sqrt($mtn)+sqrt($mtp));
	        		$this->m_topsis->insert_table_hasiltopsis(sqrt($mtp), sqrt($mtn), $V, $idk, $idp);
	        		// print_r('D+ ='.sqrt($mtp).' ');
	        		// print_r('D- ='.sqrt($mtn).' ');
	        		// print_r('V ='.$V);
	        		// echo "<br>";     
	        		//echo "<br>";  
	        		$mtn=0; 
	        		$idk=0;
	        		$idp=0;
	        		$mtp=0;      	
	        	}	
	        	$m++;
	        }


	        //v normalisasi
	        $tps = $this->m_topsis->get_hasiltopsis();
	        $dump = $this->m_topsis->sqrt_v();
	        $this->m_topsis->create_table_vnormalisasi();
	       	$t=0;
	        for ($i=0; $i < sizeof($dump); $i++) { 
	        	for ($s=0; $s < sizeof($peserta); $s++) { 
	        		$v_normal = $tps[$t]['v']/$dump[$i]['vx'];
	        		//print_r($$v_normal.' - '.$tps[$t]['idPeserta'].' -'.$dump[$i]['idKompetensi']);
	        		//echo "<br/>";
	        		$this->m_topsis->insert_table_vnormalisasi($v_normal, $tps[$s]['idPeserta'], $dump[$i]['idKompetensi']);
	        		$t++;
	        	}
	        	//echo "---------------";
	        	//echo "<br/>";
	        }


	        $tampil = array();
	        for ($l=0; $l < sizeof($peserta); $l++) { 
	        	$findmax = $this->m_topsis->find_max($peserta[$l]['idPeserta']);
	        	$tampil[] = max($findmax);
	        }
	   		
	   		//print_r($tampil);
	   		$data['hasiltopsis'] = $tampil;
	   		$this->load->view('global/header');
      	  	$this->load->view('hasil', $data);
        	$this->load->view('global/footer');

         echo '</pre>';
	}

}

?>