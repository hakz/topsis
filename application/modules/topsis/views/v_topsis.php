      <div id="page-content">
          <div id='wrap'>
              <div id="page-heading">
                  <ol class="breadcrumb">
                      <li class='active'><a href="<?php echo site_url(); ?>">Dashboard</a></li>
                  </ol>

                  <h1>Hasil Topsis <?php echo $NamaKompetensi;?></h1>
                  <div class="options">

                      <div class="btn-toolbar">
                        <?php if($id != 0){  $t = $id-1;?>
                         <a href="<?php  echo site_url('topsis/hasil/'.$t); echo '/' ?>" class="btn btn-midnightblue"><i class="icon-hand-left"></i> <?php echo $PrevNama;?></a>
                        <?php } ?>
                        <?php if($id+1 != $size){
                          $t = $id+1; ?>
                          <a href="<?php  echo site_url('topsis/hasil/'.$t); echo '/' ?>" class="btn btn-midnightblue"><?php echo $NextNama;?> <i class="icon-hand-right"></i> </a>
                        <?php } ?>

                      </div>
                  </div>
              </div>
        <div class="container">
            <div class="panel panel-sky">
            <div class="panel-heading">
                <h4>Data <?php echo $NamaKompetensi;?></h4>
            </div>
            <div class="panel-body collapse in">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>D+</th>
                            <th>D-</th>
                            <th>V</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                       $i = 0;
                       if ($hasiltopsis != null) 
                        foreach ($hasiltopsis as $dt) {  $i++; ?>
                    <tr>
                        <td class="col-md-1"><?php echo $i; ?></td>
                        <td><?php echo $dt['Nama']; ?></td>
                        <td><?php echo $dt['d_positif']; ?></td>
                        <td><?php echo $dt['d_negatif']; ?></td>
                        <td><?php echo $dt['v']; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
</div>
          </div> <!-- container -->
      </div> <!--wrap -->
  </div> <!-- page-content -->
