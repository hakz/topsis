      <div id="page-content">
          <div id='wrap'>
              <div id="page-heading">
                  <ol class="breadcrumb">
                      <li class='active'><a href="<?php echo site_url(); ?>">Dashboard</a></li>
                  </ol>

                  <h1>Hasil Topsis</h1>
              </div>
        <div class="container">
            <div class="panel panel-sky">
            <div class="panel-heading">
                <h4>Data</h4>
            </div>
            <div class="panel-body collapse in">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Kompetensi</th>
                            <th>Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                       $i = 0;
                       if ($hasiltopsis != null) 
                        foreach ($hasiltopsis as $dt) {  $i++; ?>
                    <tr>
                        <td class="col-md-1"><?php echo $i; ?></td>
                        <td><?php echo $dt['Nama']; ?></td>
                        <td><?php echo $dt['NamaKompetensi']; ?></td>
                        <td><?php echo $dt['vx']; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
</div>
          </div> <!-- container -->
      </div> <!--wrap -->
  </div> <!-- page-content -->
