<?php


if (!defined('BASEPATH'))
	exit('No direct script access allowed');


class m_topsis extends CI_Model {

	function ambilhasilbyid($id="") {
		$ambil = $this->db->query("SELECT hasil_topsis.d_positif, hasil_topsis.d_negatif, hasil_topsis.v, peserta.Nama FROM hasil_topsis JOIN peserta ON peserta.idPeserta=hasil_topsis.idPeserta WHERE hasil_topsis.idKompetensi=$id ORDER BY hasil_topsis.v DESC");
		return $ambil;
	}


	function get_hasil(){
		$hasil = $this->db->query("SELECT peserta.Nama, nilai.Nilai, kriteria.NamaKriteria, kriteria.idKriteria FROM peserta JOIN nilai ON nilai.idPeserta = peserta.idPeserta JOIN kriteria ON kriteria.idKriteria = nilai.idKriteria;");

		return $hasil->result_array();
	}

	function get_x(){
		$hasil = $this->db->query("SELECT SQRT(SUM(POW(nilai.Nilai,2))) AS 'X', kriteria.NamaKriteria FROM peserta JOIN nilai ON nilai.idPeserta = peserta.idPeserta JOIN kriteria ON kriteria.idKriteria = nilai.idKriteria GROUP BY kriteria.NamaKriteria ORDER BY kriteria.idKriteria;");

		return $hasil->result_array();
	}

	function count_orang(){
		$hasil = $this->db->query("SELECT COUNT(peserta.Nama) AS 'peserta' FROM peserta");
		return $hasil->row();
	}

	function get_peserta(){
		$hasil = $this->db->query("SELECT peserta.idPeserta, peserta.Nama AS 'peserta' FROM peserta");
		return $hasil->result_array();
	}

	function count_kompetensi(){
		$hasil = $this->db->query("SELECT COUNT(kompetensi.idKompetensi) AS 'kompetensi' FROM kompetensi");
		return $hasil->row();
	}


	function count_kriteria(){
		$hasil = $this->db->query("SELECT COUNT(kriteria.idKriteria) AS 'kriteria' FROM kriteria");
		return $hasil->row();
	}

	function bobot_kriteria(){
		$hasil = $this->db->query("SELECT kriteria.idKriteria, Bobot, kriteria.NamaKriteria, kompetensi.NamaKompetensi, kompetensi.idKompetensi FROM bobotkriteria JOIN kompetensi ON bobotkriteria.idKompetensi=kompetensi.idKompetensi JOIN kriteria ON bobotkriteria.idKriteria = kriteria.idKriteria;");

		return $hasil->result_array();
	}


	function create_table_normalisasi(){
		$this->db->query("DROP TABLE IF EXISTS `normalisasi`;");
		$this->db->query("CREATE TABLE normalisasi (idNormalisasi INT NOT NULL AUTO_INCREMENT, nilai_normalisasi DOUBLE, idPeserta INT, PRIMARY KEY(idNormalisasi));");
	}

	function insert_normalisasi($value, $id){
		$this->db->query("INSERT INTO normalisasi (nilai_normalisasi, idPeserta) VALUES ($value, $id);");
	}

	function get_normalisasi(){
		$hasil = $this->db->query("SELECT nilai_normalisasi FROM normalisasi;");
		return $hasil->result_array();
	}

	function create_table_normalisasi_terbobot(){
		$this->db->query("DROP TABLE IF EXISTS `normalisasi_terbobot`;");
		$this->db->query("CREATE TABLE normalisasi_terbobot (id INT NOT NULL AUTO_INCREMENT, nilai DOUBLE, idPeserta INT, idKriteria INT, idKompetensi INT, PRIMARY KEY(id));");
	}

	function insert_table_normalisasi_terbobot($nilai, $idPeserta, $idKompetensi, $idKriteria){
		$this->db->query("INSERT INTO normalisasi_terbobot (nilai, idPeserta, idKompetensi, idKriteria) VALUES ($nilai, $idPeserta, $idKompetensi, $idKriteria);");
	}

	function create_table_min(){
		$this->db->query("DROP TABLE IF EXISTS `min_max`;");
		$this->db->query("CREATE TABLE min_max(id INT NOT NULL AUTO_INCREMENT, min DOUBLE, max DOUBLE, idKriteria INT, idKompetensi INT, PRIMARY KEY(id));");
	}

	function insert_table_min($min, $max, $idKriteria, $idKompetensi){
		$this->db->query("INSERT INTO min_max (min, max, idKriteria, idKompetensi) VALUES ($min, $max, $idKriteria, $idKompetensi);");
	}
//surim///
	function min_max($val){
		$hasil = $this->db->query("SELECT MIN(nilai) AS 'min', MAX(nilai) AS 'max' , idKriteria, idKompetensi FROM normalisasi_terbobot WHERE idKriteria=$val GROUP BY idKompetensi;");
		return $hasil->result_array();
	}

	function min_max_normalisasi(){
			$hasil = $this->db->query("SELECT normalisasi_terbobot.id, normalisasi_terbobot.nilai, pow(nilai-min,2) AS 'pn', pow(nilai-max,2) AS 'px', normalisasi_terbobot.idPeserta, min_max.idKriteria, min_max.idKompetensi, min, max FROM normalisasi_terbobot JOIN min_max ON normalisasi_terbobot.idKompetensi=min_max.idKompetensi AND normalisasi_terbobot.idKriteria=min_max.idKriteria;");
			return $hasil->result_array();
	}

	function get_kompetensi(){
		$hasil = $this->db->query("SELECT * FROM kompetensi;");
		return $hasil->result_array();
	}

	function min_max_normal_where($val){
		$hasil = $this->db->query("SELECT normalisasi_terbobot.id, normalisasi_terbobot.nilai, pow(nilai-min,2) AS 'pn', pow(nilai-max,2) AS 'px', normalisasi_terbobot.idPeserta, min_max.idKriteria, min_max.idKompetensi, min, max FROM normalisasi_terbobot JOIN min_max ON normalisasi_terbobot.idKompetensi=min_max.idKompetensi AND normalisasi_terbobot.idKriteria=min_max.idKriteria WHERE min_max.idKompetensi=$val ORDER BY normalisasi_terbobot.idPeserta;");
			return $hasil->result_array();
	}

	function create_table_hasiltopsis(){
		$this->db->query("DROP TABLE IF EXISTS `hasil_topsis`;");
		$this->db->query("CREATE TABLE hasil_topsis (id INT NOT NULL AUTO_INCREMENT, d_positif DOUBLE, d_negatif DOUBLE, v DOUBLE, idPeserta INT, idKompetensi INT, PRIMARY KEY(id));");
	}


	function insert_table_hasiltopsis($d_positif, $d_negatif, $v, $idKompetensi, $idPeserta){
		$this->db->query("INSERT INTO hasil_topsis (d_positif, d_negatif, v, idKompetensi, idPeserta) VALUES ($d_positif, $d_negatif, $v, $idKompetensi, $idPeserta);");
	}


	function sqrt_v(){
		$hasil = $this->db->query("SELECT SQRT(sum(pow(hasil_topsis.v,2))) AS 'vx', hasil_topsis.idKompetensi, hasil_topsis.idPeserta FROM hasil_topsis GROUP BY hasil_topsis.idKompetensi");
		return$hasil->result_array();
	}

	function get_hasiltopsis(){
		$hasil = $this->db->get('hasil_topsis');
		return $hasil->result_array();
	}

	function create_table_vnormalisasi(){
		$this->db->query("DROP TABLE IF EXISTS `v_normalisasi`;");
		$this->db->query("CREATE TABLE v_normalisasi (id INT NOT NULL AUTO_INCREMENT, vx DOUBLE, idPeserta INT, idKompetensi INT, PRIMARY KEY(id));");
	}

	function insert_table_vnormalisasi($vx, $idPeserta, $idKompetensi){
		$this->db->query("INSERT INTO v_normalisasi (vx, idPeserta, idKompetensi) VALUES ($vx, $idPeserta, $idKompetensi);");
	}

	function find_max($id){
		$hasil = $this->db->query("SELECT v_normalisasi.vx, v_normalisasi.idPeserta, peserta.Nama, v_normalisasi.idKompetensi, kompetensi.NamaKompetensi FROM v_normalisasi JOIN kompetensi ON kompetensi.idKompetensi = v_normalisasi.idKompetensi JOIN peserta ON peserta.idPeserta=v_normalisasi.idPeserta WHERE v_normalisasi.idPeserta=$id");
		return  $hasil->result_array();
	}




	///FUNGSI SELECT INTERFACE
		function ambil_kompetensi() {
		$ambil = $this->db->get('kompetensi');
		if ($ambil->num_rows() > 0) {
			foreach ($ambil->result() as $data) {
				$hasil[] = $data;
			}
			return $hasil;
		}
	}


	function ambil_kriteria() {
		$ambil = $this->db->get('kriteria');
		if ($ambil->num_rows() > 0) {
			foreach ($ambil->result() as $data) {
				$hasil[] = $data;
			}
			return $hasil;
		}
	}
}


?>