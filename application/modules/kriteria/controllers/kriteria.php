<?php

class kriteria extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url', 'form'));
		$this->load->model('m_kriteria');
	}

	function index(){
		$data['data'] = $this->m_kriteria->ambil();
		$this->load->view('global/header');
        $this->load->view("index", $data);
        $this->load->view('global/footer');
	}

	function tambah(){
			$d['tipe'] = 'Tambah';
			$d['NamaKriteria'] = "";
			$d['id'] = "";
			$this->load->view('global/header');
			$this->load->view('kriteria/tambah', $d);
			$this->load->view('global/footer');

	}


	function edit($id=''){
			$ambil = $this->m_kriteria->ambilbyid($id)->row();
			$d['NamaKriteria'] = $ambil->NamaKriteria;
			$d['tipe'] = 'Edit';
			$d['id'] = $id;
			$this->load->view('global/header');
			$this->load->view('kriteria/tambah', $d);
			$this->load->view('global/footer');
	}

		function simpan(){
			$this->form_validation->set_rules('nama', 'Nama Kriteria', 'alpha|required');
			if ($this->input->post('submit')) {
				if ($this->form_validation->run() == FALSE) {
					$data['NamaKriteria'] = $this->input->post('nama');
					$data['id'] = $this->input->post('id');
					$data['tipe'] = $this->input->post('tipe');
					$this->load->view('global/header');
					$this->load->view('kriteria/tambah', $data);
					$this->load->view('global/footer');
				}else{
					if( $this->input->post('tipe')== 'Tambah'){
						$data['Nama'] = $this->input->post('nama');
						print_r($data['Nama']);
						//$this->m_penyakit->tambah_gejala();
						//redirect('peserta/tembah_nilai/');
					}else{
						$id = $this->input->post('id');
						print_r($id.'=>'.$data['Nama']);
						//$this->m_penyakit->update_gejala($id_gejala);
						//redirect('penyakit/edit_gejala/'.$id_gejala);
					}
				}
			}
		
	}
}
?>