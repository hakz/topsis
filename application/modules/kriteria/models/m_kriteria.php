<?php


if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class m_kriteria extends CI_Model {

	function ambil() {
		$ambil = $this->db->get('kriteria');
		if ($ambil->num_rows() > 0) {
			foreach ($ambil->result() as $data) {
				$hasil[] = $data;
			}
			return $hasil;
		}
	}

	function ambilbyid($id="") {
		$ambil = $this->db->get_where('kriteria', array('idKriteria' => $id));
		return $ambil;
	}


}

?>