<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
    <title>TOPSIS</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Administrator Dinas Peternakan Kabupaten Tapin" />
    <meta name="author" content="Administrator Dinas Peternakan Kabupaten Tapin" />

    <link href="<?php echo base_url();?>assets/css/styles.min.css" rel="stylesheet" type='text/css' media="all" />
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600' rel='stylesheet' type='text/css' />


    <link href='<?php echo base_url();?>assets/demo/variations/default.css' rel='stylesheet' type='text/css' media='all' id='styleswitcher' />

    <link href='<?php echo base_url();?>assets/demo/variations/default.css' rel='stylesheet' type='text/css' media='all' id='headerswitcher' />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
	<!--[if lt IE 9]>
		<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/charts-flot/excanvas.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ie8.css">
        <![endif]-->

        <!-- The following CSS are included as plugins and can be removed if unused-->
        <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>assets/plugins/form-select2/select2.css' />
        <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>assets/plugins/form-multiselect/css/multi-select.css' />
        <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>assets/plugins/form-toggle/toggles.css' />
        <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>assets/plugins/codeprettifier/prettify.css' />
        <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>assets/plugins/form-daterangepicker/daterangepicker-bs3.css' />
        <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>assets/plugins/fullcalendar/fullcalendar.css' />
        <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>assets/plugins/form-markdown/css/bootstrap-markdown.min.css' />
        <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>assets/plugins/datatables/dataTables.css' />
        <link rel='stylesheet' type='text/css' href='<?php echo base_url();?>assets/js/jqueryui.css' />

        <style>
        </style>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

        <header class="navbar navbar-inverse navbar-fixed-top" role="banner">
            <a id="leftmenu-trigger" class="pull-left" data-toggle="tooltip" data-placement="bottom" title="Toggle Left Sidebar"></a>


            <div class="navbar-header pull-left">
                <h4 style="color:white;">TOPSIS </h4>
            </div>


   </header>

   <div id="page-container">
    <!-- BEGIN SIDEBAR -->
    <nav id="page-leftbar" role="navigation">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="acc-menu" id="sidebar">
            <li id="search">
                <a href="javascript:;"><i class="icon-search opacity-control"></i></a>
                <form />
                <input type="text" class="search-query" placeholder="" disabled/>
            </form>
        </li>
        <li class="divider"></li>
        <li><a href="<?php echo site_url(); ?>"><i class="icon-home"></i> <span>Topsis</span></a></li>
        <li><a href="<?php echo site_url('topsis/prosestopsis'); ?>"><i class="icon-list-alt"></i> <span>Hasil Topsis</span></a></li>
                   <li><a href="javascript:;"><i class="icon-list-ol"></i> <span>Peserta</span></a>
                        <ul class='acc-menu'>
                            <li><a href="<?php echo site_url('peserta');?>">Data Peserta</a></li>
                            <li><a href="<?php echo site_url('peserta/nilai');?>">Data Nilai</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:;"><i class="icon-tasks"></i> <span>Kriteria</span></a>
                        <ul class="acc-menu">
                            <li><a href="<?php echo site_url('kriteria');?>">Data Kriteria</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:;"><i class="icon-table"></i> <span>Kompetensi</span></a>
                        <ul class="acc-menu">
                            <li><a href="<?php echo site_url('kompetensi');?>">Data Kompetensi</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo site_url('topsis/tambah_bobot'); ?>"><i class="icon-indent-right"></i> <span>Tambah Bobot</span></a></li>
                </li></ul>
                <!-- END SIDEBAR MENU -->
            </nav>
