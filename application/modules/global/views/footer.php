
<footer role="contentinfo">
    <div class="clearfix">
        <ul class="list-unstyled list-inline">
            <li>Developed by hafizridha.net</li>
            <!--li class="pull-right"><a href="javascript:;" id="back-to-top">Top <i class="icon-arrow-up"></i></a></li-->
            <button class="pull-right btn btn-inverse btn-xs" id="back-to-top" style="margin-top: -1px; text-transform: uppercase;"><i class="icon-arrow-up"></i></button>
        </ul>
    </div>
</footer>

</div> <!-- page-container -->

<!--
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script>!window.jQuery && document.write(unescape('%3Cscript src="assets/js/jquery-1.10.2.min.js"%3E%3C/script%3E'))</script>
<script type="text/javascript">!window.jQuery.ui && document.write(unescape('%3Cscript src="assets/js/jqueryui-1.10.3.min.js'))</script>
-->



<script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery-1.10.2.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/js/jqueryui-1.10.3.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/js/bootstrap.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/js/enquire.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.cookie.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.touchSwipe.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/js/jquery.nicescroll.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/codeprettifier/prettify.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/quicksearch/jquery.quicksearch.min.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/form-multiselect/js/jquery.multi-select.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/sparklines/jquery.sparklines.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/form-toggle/toggle.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/form-wysihtml5/wysihtml5-0.3.0.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/form-wysihtml5/bootstrap-wysihtml5.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/fullcalendar/fullcalendar.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/form-daterangepicker/daterangepicker.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/form-daterangepicker/moment.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/form-select2/select2.min.js'></script>
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/form-autosize/jquery.autosize-min.js'></script>  
<script type='text/javascript' src='<?php echo base_url();?>assets/demo/demo-datatables.js'></script>  
<script type='text/javascript' src='<?php echo base_url();?>assets/demo/demo.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/form-inputmask/jquery.inputmask.bundle.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/demo/demo-mask.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/js/placeholdr.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/js/application.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/plugins/form-jasnyupload/fileinput.min.js'></script> 
<script type='text/javascript' src='<?php echo base_url();?>assets/demo/demo-formcomponents.js'></script> 

</body>
</html>