<?php

class kompetensi extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url', 'form'));
		$this->load->model('m_kompetensi');
	}

	function index(){
		$data['data'] = $this->m_kompetensi->ambil();
		$this->load->view('global/header');
        $this->load->view("index", $data);
        $this->load->view('global/footer');
	}

	function tambah(){
			$d['tipe'] = 'Tambah';
			$d['NamaKompetensi'] = "";
			$d['id'] = "";
			$this->load->view('global/header');
			$this->load->view('kompetensi/tambah', $d);
			$this->load->view('global/footer');

	}


	function edit($id=''){
			$ambil = $this->m_kompetensi->ambilbyid($id)->row();
			$d['NamaKompetensi'] = $ambil->NamaKompetensi;
			$d['tipe'] = 'Edit';
			$d['id'] = $id;
			$this->load->view('global/header');
			$this->load->view('kompetensi/tambah', $d);
			$this->load->view('global/footer');
	}

	function simpan(){
			$this->form_validation->set_rules('nama', 'Nama Kompetensi', 'alpha|required');
			if ($this->input->post('submit')) {
				if ($this->form_validation->run() == FALSE) {
					$data['NamaKompetensi'] = $this->input->post('nama');
					$data['id'] = $this->input->post('id');
					$data['tipe'] = $this->input->post('tipe');
					$this->load->view('global/header');
					$this->load->view('kompetensi/tambah', $data);
					$this->load->view('global/footer');
				}else{
					if( $this->input->post('tipe')== 'Tambah'){
						$data['NamaKompetensi'] = $this->input->post('nama');
						print_r($data['Nama']);
						//$this->m_penyakit->tambah_gejala();
						//redirect('peserta/tembah_nilai/');
					}else{
						$id = $this->input->post('id');
						print_r($id.'=>'.$data['Nama']);
						//$this->m_penyakit->update_gejala($id_gejala);
						//redirect('penyakit/edit_gejala/'.$id_gejala);
					}
				}
			}
		
	}
}
?>