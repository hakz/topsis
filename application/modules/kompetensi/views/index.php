<div id="page-content">
    <div id='wrap'>
        <div id="page-heading">
            <ol class="breadcrumb">
                <li><a href="index.php">Dashboad</a></li>
                <li>Data Kompetensi</li>
            </ol>

            <h1>Data Kompetensi</h1>
        </div>


        <div class="container">
         <a href="<?php echo site_url('kompetensi/tambah'); ?>" class="btn btn-midnightblue"><i class="icon-plus"></i> Tambah Data</a>
         <br />
         <br />
         <div class="row">
          <div class="col-md-12">
           <?php if (validation_errors()) { ?>
           <div class="alert alert-dismissable alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4>Terjadi Kesalahan!</h4>
              <?php echo validation_errors(); ?>
          </div>
          <?php } ?>
          <?php if ($this->session->flashdata('result_pass')) { ?>
          <div class="alert alert-dismissable alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4>Terjadi Kesalahan!</h4>
              <?php echo $this->session->flashdata('result_pass'); ?>
          </div>
          <?php } ?>
          <?php if ($this->session->flashdata('result_pass_true')) { ?>
          <div class="alert alert-dismissable alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4>Sukses!</h4>
              <?php echo $this->session->flashdata('result_pass_true'); ?>
          </div>
          <?php } ?>
          <div class="panel panel-sky">
            <div class="panel-heading">
                <h4>Data Peserta</h4>
            </div>
            <div class="panel-body collapse in">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="example">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                       $i = 0;
                       if ($data != null) 
                        foreach ($data as $dt) {  $i++; ?>
                    <tr>
                        <td class="col-md-1"><?php echo $i; ?></td>
                        <td class="col-md-8"><?php echo $dt->NamaKompetensi; ?></td>
                        <td class="col-md-2">
                         <div class="btn-group">
                             <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                Aksi <span class="caret"></span>
                            </button>                                <ul class="dropdown-menu" role="menu">
                            <li><a href="<?php echo site_url('kompetensi/edit').'/'.$dt->idKompetensi; ?>">Edit</a></li>
                            <li><a href="<?php echo site_url('kompetensi/hapus').'/'.$dt->idKompetensi; ?>" onclick="return confirm('Anda yakin menghapus kompetensi?');">Hapus</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
</div>
</div>
</div>

</div> <!-- container -->
</div> <!--wrap -->
</div> <!-- page-content -->