<?php


if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class m_kompetensi extends CI_Model {

	function ambil() {
		$ambil = $this->db->get('kompetensi');
		if ($ambil->num_rows() > 0) {
			foreach ($ambil->result() as $data) {
				$hasil[] = $data;
			}
			return $hasil;
		}
	}


	function ambilbyid($id="") {
		$ambil = $this->db->get_where('kompetensi', array('IdKompetensi' => $id));
		return $ambil;
	}



}

?>