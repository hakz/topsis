<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- STYLER -->

                            <!-- /STYLER -->
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <a href="#">Other Pages</a>
                                </li>
                                <li>Error 404</li>
                            </ul>
                            <!-- /BREADCRUMBS -->
                            <div class="clearfix">
                                <h3 class="content-title pull-left">Error 404</h3>
                            </div>
                            <div class="description">Error 404 Page</div>
                        </div>
                    </div>
                </div>
                <!-- /PAGE HEADER -->
                <div class="row">
                    <div class="col-md-12 not-found text-center">
                        <div class="error">
                            404
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-4 not-found text-center">
                        <div class="content">
                            <h3>Page not Found</h3>
                            <p>
                                Sorry, but the page you're looking for has not been found<br />
                                Try checking the URL for errors, <a href="index.html">goto home</a> or try to search below.
                            </p>
                            <form action="#" />
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="search here..." />
                                <span class="input-group-btn">                   
                                    <button type="submit" class="btn btn-success"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

	<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
        <script src="<?php echo base_url('assets/js/jquery/jquery-2.0.3.min.js');?>"></script>
	<!-- JQUERY UI-->
	<script src="<?php echo base_url('assets/js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js');?>"></script>
	<!-- BOOTSTRAP -->
	<script src="<?php echo base_url('assets/bootstrap-dist/js/bootstrap.min.js');?>"></script>
	
		
	<!-- DATE RANGE PICKER -->
	<script src="<?php echo base_url('assets/js/bootstrap-daterangepicker/moment.min.js');?>"></script>
	
	<script src="<?php echo base_url('assets/js/bootstrap-daterangepicker/daterangepicker.min.js');?>"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js');?>"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jQuery-Cookie/jquery.cookie.min.js');?>"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="<?php echo base_url('assets/js/script.js');?>"></script>
	<script>
		jQuery(document).ready(function() {		
			App.setPage("widgets_box");  //Set current page
			App.init(); //Initialise plugins and elements
		});
	</script>
	<!-- /JAVASCRIPTS -->