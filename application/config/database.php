<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;
//postgre
/*$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'postgres';
$db['default']['password'] = 'postgres';
$db['default']['database'] = 'si_arsip';
$db['default']['dbdriver'] = 'postgre';*/

// ORACLE BANTUL
/*$db['default']['hostname'] = "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.16.217)(PORT=1521))(CONNECT_DATA=(SID=SISMIOP)))";
$db['default']['username'] = 'SIARSIP';
$db['default']['password'] = 'SIARSIP';
$db['default']['database'] = '';
$db['default']['dbdriver'] = 'oci8';
$db['default']['port'] = 1521;
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;*/

// $db['default']['hostname'] = "192.168.16.217/SISMIOP";
// $db['default']['username'] = "SIARSIP";
// $db['default']['password'] = "SIARSIP";
// $db['default']['database'] = "";
// $db['default']['dbdriver'] = "oci8";
// $db['default']['dbprefix'] = "";
// $db['default']['pconnect'] = FALSE; //must be false
// $db['default']['db_debug'] = TRUE;
// $db['default']['cache_on'] = FALSE;
// $db['default']['cachedir'] = "";
// $db['default']['char_set'] = "utf8";
// $db['default']['dbcollat'] = "utf8_general_ci";

//oracle
/*$db['default']['username'] = 'si_arsip';
$db['default']['password'] = 'milikita';
$db['default']['database'] = '';
$db['default']['dbdriver'] = 'oci8'; //must be actived in php.ini
$db['default']['port'] = 1521;*/
//simpatda database


$active_group = 'default';
$active_record = TRUE;

$db['default']['hostname'] = 'localhost';
$db['default']['username'] = 'root';
$db['default']['password'] = '';
$db['default']['database'] = 'topsis';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;


// $db['simpatda']['hostname'] = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.16.217)(PORT=1521))(CONNECT_DATA=(SID=SISMIOP)))';
// $db['simpatda']['username'] = 'SIMPATDA';
// $db['simpatda']['password'] = 'SIMPATDA';
// $db['simpatda']['database'] = '';
// $db['simpatda']['dbdriver'] = 'oci8';
// $db['simpatda']['port'] = 1521;
// $db['simpatda']['dbprefix'] = '';
// $db['simpatda']['pconnect'] = TRUE;
// $db['simpatda']['db_debug'] = TRUE;
// $db['simpatda']['cache_on'] = FALSE;
// $db['simpatda']['cachedir'] = '';
// $db['simpatda']['char_set'] = 'utf8';
// $db['simpatda']['dbcollat'] = 'utf8_general_ci';
// $db['simpatda']['swap_pre'] = '';
// $db['simpatda']['autoinit'] = TRUE;
// $db['simpatda']['stricton'] = FALSE;

// $db['pbb']['hostname'] = '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.16.217)(PORT=1521))(CONNECT_DATA=(SID=SISMIOP)))';
// $db['pbb']['username'] = 'PBB';
// $db['pbb']['password'] = 'PBB';
// $db['pbb']['database'] = '';
// $db['pbb']['dbdriver'] = 'oci8';
// $db['pbb']['port'] = 1521;
// $db['pbb']['dbprefix'] = '';
// $db['pbb']['pconnect'] = TRUE;
// $db['pbb']['db_debug'] = TRUE;
// $db['pbb']['cache_on'] = FALSE;
// $db['pbb']['cachedir'] = '';
// $db['pbb']['char_set'] = 'utf8';
// $db['pbb']['dbcollat'] = 'utf8_general_ci';
// $db['pbb']['swap_pre'] = '';
// $db['pbb']['autoinit'] = TRUE;
// $db['pbb']['stricton'] = FALSE;


/* End of file database.php */
/* Location: ./application/config/database.php */