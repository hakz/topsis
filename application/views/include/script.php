<meta charset="utf-8" />
<title><?=(isset($title)) ? $title.' | ' : 'SI ARSIP KAB. BANTUL';?></title>
<meta name="description" content="overview & stats" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- basic styles -->
<link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?=base_url()?>assets/css/bootstrap-responsive.min.css" rel="stylesheet" />

<link rel="stylesheet" href="<?=base_url()?>assets/css/font-awesome.min.css" />

<!-- ace styles -->
<link rel="stylesheet" href="<?=base_url()?>assets/css/ace.min.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/ace-responsive.min.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/ace-skins.min.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery-ui-1.10.2.custom.min.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.treeview.css" />
<link rel="stylesheet" href="<?=base_url()?>assets/css/datepicker.css" />

<!-- basic scripts -->
<script src="<?=base_url()?>assets/js/jquery-1.7.2.js"></script>

<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery-ui-1.10.2.custom.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.easy-pie-chart.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.flot.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.flot.pie.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/jquery.treeview.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/fuelux.wizard.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<!-- ace scripts -->
<script src="<?=base_url()?>assets/js/ace-elements.min.js"></script>
<script src="<?=base_url()?>assets/js/ace.min.js"></script>

<!-- datatable -->
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.dataTables.bootstrap.js"></script>