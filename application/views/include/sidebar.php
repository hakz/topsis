<?php
	$menus = array();
	$submenu = array();
	if($this->session->userdata('id_group') == 1){
		$menus[] = array('title' => 'Dashboard', 'url' => base_url('dashboard') ,'submenu' => $submenu, 'icon' => 'icon-dashboard');

		$submenu = array();
		$menus[] = array('title' => 'Pencarian Arsip', 'url' => base_url('pencarian') ,'submenu' => $submenu, 'icon' => 'icon-search');

		$submenu = array();
		$submenu[] = array('url' => base_url('master/klasifikasi'), 'title' => 'Klasifikasi Arsip');
		$submenu[] = array('url' => base_url('master/gedung'), 'title' => 'Lokasi');
		$submenu[] = array('url' => base_url('master/instansi'), 'title' => 'Instansi & Unit Kerja');
		$submenu[] = array('url' => base_url('master/kondisi'), 'title' => 'Kondisi Fisik');
		$submenu[] = array('url' => base_url('master/media'), 'title' => 'Media');
		$submenu[] = array('url' => base_url('master/status'), 'title' => 'Status');
		$menus[] = array('title' => 'Master', 'url' => '#' ,'submenu' => $submenu, 'icon' => 'icon-list');

		$submenu = array();
		$submenu[] = array('url' => base_url('arsip'), 'title' => 'Bundling');
		$submenu[] = array('url' => base_url('arsip/box'), 'title' => 'Boxing');
		$submenu[] = array('url' => base_url('arsip/shelf'), 'title' => 'Shelfing');
		$menus[] = array('title' => 'Pengarsipan', 'url' => '#', 'submenu' => $submenu, 'icon' => 'icon-book');

		$submenu = array();
		$submenu[] = array('url' => base_url('transaksi/keluar'), 'title' => 'Arsip Keluar');
		$submenu[] = array('url' => base_url('transaksi/kembali'), 'title' => 'Arsip Kembali');
		$submenu[] = array('url' => base_url('transaksi/retensi'), 'title' => 'Retensi');
		$menus[] = array('title' => 'Transaksi Arsip', 'url' => '#', 'submenu' => $submenu, 'icon' => 'icon-share');

		$submenu = array();
		$submenu[] = array('url' => base_url('daur_hidup/jadwal_retensi'), 'title' => 'Jadwal Retensi');
		/*$submenu[] = array('url' => base_url('daur_hidup/mutasi'), 'title' => 'Mutasi');
		$submenu[] = array('url' => base_url('daur_hidup/pemusnahan'), 'title' => 'Pemusnahan');
		$submenu[] = array('url' => base_url('daur_hidup/peninjauan'), 'title' => 'Peninjauan Ulang');*/
		$submenu[] = array('url' => base_url('#construct'), 'title' => 'Mutasi');
		$submenu[] = array('url' => base_url('#construct'), 'title' => 'Pemusnahan');
		$submenu[] = array('url' => base_url('#construct'), 'title' => 'Peninjauan Ulang');
		$menus[] = array('title' => 'Daur Hidup Arsip','url' => '#', 'submenu' => $submenu, 'icon' => 'icon-refresh');

		$submenu = array();
		$submenu[] = array('url' => base_url('laporan/histori'), 'title' => 'Histori');
		$menus[] = array('title' => 'Laporan','url' => '#', 'submenu' => $submenu, 'icon' => 'icon-file');

		$submenu = array();
		$submenu[] = array('url' => base_url('setting/user'), 'title' => 'Manajemen User');
		$menus[] = array('title' => 'Setting','url' => '#', 'submenu' => $submenu, 'icon' => 'icon-edit');

		$submenu = array();
		$menus[] = array('title' => 'Log out','url' => base_url('login/logout'), 'submenu' => $submenu, 'icon' => 'icon-off');
	}else{
		$menus[] = array('title' => 'Dashboard', 'url' => base_url('dashboard') ,'submenu' => $submenu, 'icon' => 'icon-dashboard');

		$submenu = array();
		$menus[] = array('title' => 'Pencarian Arsip', 'url' => base_url('pencarian') ,'submenu' => $submenu, 'icon' => 'icon-search');

		$submenu = array();
		$submenu[] = array('url' => base_url('arsip'), 'title' => 'Bundling');
		$submenu[] = array('url' => base_url('arsip/box'), 'title' => 'Boxing');
		$submenu[] = array('url' => base_url('arsip/shelf'), 'title' => 'Shelfing');
		$menus[] = array('title' => 'Pengarsipan', 'url' => '#', 'submenu' => $submenu, 'icon' => 'icon-book');

		$submenu = array();
		$submenu[] = array('url' => base_url('transaksi/keluar'), 'title' => 'Arsip Keluar');
		$submenu[] = array('url' => base_url('transaksi/kembali'), 'title' => 'Arsip Kembali');
		$submenu[] = array('url' => base_url('transaksi/retensi'), 'title' => 'Retensi');
		$menus[] = array('title' => 'Transaksi Arsip', 'url' => '#', 'submenu' => $submenu, 'icon' => 'icon-share');
		
		$submenu = array();
		$menus[] = array('title' => 'Log out','url' => base_url('login/logout'), 'submenu' => $submenu, 'icon' => 'icon-off');
	}
?>

<a href="#" id="menu-toggler"><span></span></a><!-- menu toggler -->
<div id="sidebar">
	<div id="sidebar-shortcuts">
		<div id="sidebar-shortcuts-large" style="padding-bottom:0px">
			<h5>Main Menu</h5>
		</div>
		<div id="sidebar-shortcuts-mini">
			<span class="btn btn-success"></span>
			<span class="btn btn-info"></span>
			<span class="btn btn-warning"></span>
			<span class="btn btn-danger"></span>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			$('.submenu li a').click(function(){
				if($(this).attr('href') == '<?php echo base_url() ?>#construct'){
					alert('Under Construction!'+"\n"+'Maaf, fitur ini masih dalam tahap pengembangan!');
					return false;
				}
			})	
		})
		
	</script>
	<ul class="nav nav-list">
		<?php
			$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			foreach ($menus as $menu) {
				$class = "";
				$is_active = "";
				if(count($menu['submenu']) > 0){
					$submenu = "";
					foreach ($menu['submenu'] as $i => $v) {
						$class = "";
						if($actual_link == $v['url']){
							$class = "active";
							$is_active = "active open";
						}
						$submenu .= '<li class="'.$class.'"><a href="'.$v['url'].'" ><i class="icon-double-angle-right"></i>'.$v['title'].'</a></li>';
					}

					$li = '<li class="'.$is_active.'">
						<a href="'.$menu['url'].'" class="dropdown-toggle">
							<i class="'.$menu['icon'].'"></i>
							<b class="arrow icon-angle-down"></b>
							<span>'.$menu['title'].'</span>
						</a>
						<ul class="submenu">'.$submenu.'</ul>
					</li>';
					echo $li;

				}else{
					if($actual_link == $menu['url']){
						$class = "active";
					}
					echo '<li id="dashboard" class="'.$class.'">
						<a href="'.$menu['url'].'">
							<i class="'.$menu['icon'].'"></i>
							<span>'.$menu['title'].'</span>
						</a>
					</li>';
				}
			}
		?>
	</ul>
	<div id="sidebar-collapse"><i class="icon-double-angle-left"></i></div>
</div>