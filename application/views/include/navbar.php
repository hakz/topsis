<div class="navbar navbar-inverse">
	<!-- <div class="navbar-inner">
		<img src="<?php echo base_url()?>/assets/img/dppkad.jpg" style="height:80px"/>
	</div> -->
	<div class="navbar-inner">
		<div class="container-fluid">
			<a class="brand" href="#"><img src="<?php echo base_url().'assets/img/header.png'?>" /></a>
			<ul class="nav ace-nav pull-right">
				<li class="purple">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-bell-alt icon-animated-bell icon-only"></i>
						<span class="badge badge-important">8</span>
					</a>
					<ul class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-closer">
						<li class="nav-header">
							<i class="icon-warning-sign"></i> 8 Notifications	
						</li>
						<li>
							<a href="#">
								<div class="clearfix">
									<span class="pull-left"><i class="icon-comment btn btn-mini btn-pink"></i> Arsip Retensi</span>
									<span class="pull-right badge badge-info">+12</span>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="icon-user btn btn-mini btn-primary"></i> Dokumen Belum Kembali <span class="pull-right badge badge-success">+8</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="light-blue user-profile">
					<a class="user-menu dropdown-toggle" href="#" data-toggle="dropdown">
						<img src="<?=base_url()?>assets/avatars/user.jpg" class="nav-user-photo" />
						<span id="user_info">
							<small>Welcome,</small> <?php echo $this->session->userdata('nama_user') ?>
						</span>
						<i class="icon-caret-down"></i>
					</a>
					<ul id="user_menu" class="pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
						<li><a href="<?=base_url()?>user/profil"><i class="icon-user"></i> Profile</a></li>
						<li class="divider"></li>
						<li><a href="<?=base_url()?>login/logout"><i class="icon-off"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>