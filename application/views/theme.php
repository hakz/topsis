<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>PT Tata Karya Gemilang | <?php
            if ($this->uri->segment(1) == '') {
                echo 'Dashboard';
            }
            else
                echo $this->uri->segment(1);
            ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- STYLESHEETS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/cloud-admin.css'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/themes/default.css'); ?>" id="skin-switcher" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/responsive.css'); ?>" />

        <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" />
        <!-- ANIMATE -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/animateassets/css/animate.min.css'); ?>" />
        <!-- DATE RANGE PICKER -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css'); ?>" />
        <!-- TODO -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/jquery-todo/assets/css/styles.css'); ?>" />
        <!-- FULL CALENDAR -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/fullcalendar/fullcalendar.min.css'); ?>" />
        <!-- GRITTER -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/gritter/assets/css/jquery.gritter.css'); ?>" />
        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css' />
        <!-- TYPEAHEAD -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/typeahead/typeahead.css'); ?>" />
        <!-- FILE UPLOAD -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/bootstrap-fileupload/bootstrap-fileupload.min.css'); ?>" />
        <!-- SELECT2 -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/select2/select2.min.css'); ?>" />
        <!-- UNIFORM -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/uniform/css/uniform.default.min.css'); ?>" />
        <!-- JQUERY UPLOAD -->
        <!-- blueimp Gallery styles -->
        <link rel="stylesheet" href="<?php echo base_url('assets/js/blueimp/gallery/blueimp-gallery.min.css'); ?>" />
        <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
        <link rel="stylesheet" href="<?php echo base_url('assets/js/jquery-upload/css/jquery.fileupload.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/js/jquery-upload/css/jquery.fileupload-ui.css'); ?>" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
    <body>
        <!-- HEADER -->
        <header class="navbar clearfix" id="header">
            <div class="container">
                <div class="navbar-brand">
                    <!-- COMPANY LOGO -->
                    <a href="<?php echo site_url(); ?>">
                        <img  src="<?php echo base_url('assets/img/logo/logo.png') ?>" alt="Cloud Admin Logo" class="img-responsive" />
                    </a>
                    <!-- /COMPANY LOGO -->
                    <!-- PROJECT SWITCHER FOR MOBILE -->
                    <div class="visible-xs">
                        <a href="#" class="project-switcher-btn switcher btn dropdown-toggle tip-bottom" data-toggle="tooltip" title="Toggle Team View">
                            <i class="fa fa-group"></i>
                        </a>
                    </div>
                    <!-- /PROJECT SWITCHER FOR MOBILE -->
                    <!-- SIDEBAR COLLAPSE -->
                    <div id="sidebar-collapse" class="sidebar-collapse btn tip-bottom" data-toggle="tooltip" title="Toggle Sidebar">
                        <i class="fa fa-reorder" data-icon1="fa fa-reorder" data-icon2="fa fa-reorder"></i>
                    </div>
                    <!-- /SIDEBAR COLLAPSE -->
                </div>

                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN NOTIFICATION DROPDOWN -->	

                    <!-- END NOTIFICATION DROPDOWN -->
                    <!-- BEGIN INBOX DROPDOWN -->

                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown user" id="header-user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img alt="" src="assets/img/avatars/avatar3.jpg" />
                            <span class="username">Administrator</span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="fa fa-cog"></i> Account Settings</a></li>
                            <li><a href="<?php echo base_url("login/logout"); ?>"><i class="fa fa-power-off"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </header>
        <!--/HEADER -->
        <!-- PAGE -->
        <section id="page">
            <!-- SIDEBAR -->
            <div id="sidebar" class="sidebar">
                <div class="sidebar-menu nav-collapse">
                    <!-- SIDEBAR MENU -->
                    <ul>
                        <li class="has-sub <?php echo ($this->uri->segment(1) == 'news' && $this->uri->segment(2) == '' OR $this->uri->segment(2) == 'tambah_news' OR $this->uri->segment(2) == 'edit_news') ? 'active' : ''; ?>">
                            <a href="<?php echo site_url('news'); ?> ">
                                <i class="fa fa-rss fa-fw"></i> <span class="menu-text">News</span>
                                <span class="selected"></span>
                            </a>					
                        </li>
<!--                        <li class="has-sub <?php // echo ($this->uri->segment(1) == 'news' && $this->uri->segment(2) == '' OR $this->uri->segment(2) == 'tambah_news' OR $this->uri->segment(2) == 'edit_news') ? 'active' : '';   ?>">
                            <a href="javascript:;" class="">
                                <i class="fa fa-rss fa-fw"></i> <span class="menu-text">News</span>
                                <span class="arrow <?php // echo ($this->uri->segment(1) == 'news' && $this->uri->segment(2) == '' OR $this->uri->segment(2) == 'tambah_news' OR $this->uri->segment(2) == 'edit_news') ? 'open selected' : '';   ?>"></span>
                            </a>
                            <ul class="sub">
                                <li class="<?php // echo ($this->uri->segment(1) == 'news' && $this->uri->segment(2) == '') ? 'active' : '';   ?>"><a class="" href="<?php // echo site_url('news')   ?>"><span class="sub-menu-text">List News</span></a></li>
                                <li class="<?php // echo ($this->uri->segment(1) == 'news' && $this->uri->segment(2) == 'tambah_news') ? 'active' : '';   ?>"><a href="<?php // echo site_url('news/tambah_news')   ?>"><span class="sub-menu-text">Tambah News</span></a></li>
                            </ul>
                        </li>-->
                        <li class="has-sub <?php echo ($this->uri->segment(1) == 'manajemen' && $this->uri->segment(2) == '' OR $this->uri->segment(2) == 'tambah_manajemen' OR $this->uri->segment(2) == 'edit_manajemen' OR $this->uri->segment(2) == 'hak_akses_manajemen' OR $this->uri->segment(2) == 'lihat_jabatan' OR $this->uri->segment(2) == 'tambah_jabatan' OR $this->uri->segment(2) == 'edit_jabatan') ? 'active' : ''; ?>">
                            <a href="javascript:;" class="">
                                <i class="fa fa-list fa-fw"></i> <span class="menu-text">Data Manajemen</span>
                                <span class="arrow <?php echo ($this->uri->segment(1) == 'manajemen' && $this->uri->segment(2) == '' OR $this->uri->segment(2) == 'tambah_manajemen' OR $this->uri->segment(2) == 'edit_manajemen' OR $this->uri->segment(2) == 'hak_akses_manajemen' OR $this->uri->segment(2) == 'lihat_jabatan' OR $this->uri->segment(2) == 'tambah_jabatan' OR $this->uri->segment(2) == 'edit_jabatan') ? 'open selected' : ''; ?>"></span>
                            </a>
                            <ul class="sub">
                                <li class="<?php echo ($this->uri->segment(1) == 'manajemen' && $this->uri->segment(2) == '') ? 'active' : ''; ?>"><a class="" href="<?php echo site_url('manajemen') ?>"><span class="sub-menu-text">List Manajemen</span></a></li>
                                <!--<li class="<?php // echo ($this->uri->segment(1) == 'manajemen' && $this->uri->segment(2) == 'tambah_manajemen') ? 'active' : '';   ?>"><a class="" href="<?php // echo site_url('manajemen/tambah_manajemen');   ?>"><span class="sub-menu-text">Tambah Manajemen</span></a></li>-->
                                <li class="<?php echo ($this->uri->segment(1) == 'manajemen' && $this->uri->segment(2) == 'lihat_jabatan') ? 'active' : ''; ?>"><a class="" href="<?php echo site_url('manajemen/lihat_jabatan'); ?>"><span class="sub-menu-text">List Jabatan</span></a></li>
                                <!--<li class="<?php // echo ($this->uri->segment(1) == 'manajemen' && $this->uri->segment(2) == 'tambah_jabatan') ? 'active' : '';   ?>"><a class="" href="<?php // echo site_url('manajemen/tambah_jabatan');   ?>"><span class="sub-menu-text">Tambah Jabatan</span></a></li>-->
                                <li class="<?php echo ($this->uri->segment(1) == 'manajemen' && $this->uri->segment(2) == 'hak_akses_manajemen') ? 'active' : ''; ?>"><a class="" href="<?php echo site_url('manajemen/hak_akses_manajemen'); ?>"><span class="sub-menu-text">Hak Akses Manajemen</span></a></li>
                            </ul>
                        </li>
                        <li class="has-sub <?php echo ($this->uri->segment(1) == 'operator' && $this->uri->segment(2) == '' OR $this->uri->segment(2) == 'tambah_operator' OR $this->uri->segment(2) == 'edit_operator') ? 'active' : ''; ?>">
                            <a href="<?php echo site_url('operator'); ?> ">
                                <i class="fa fa-user-md fa-fw"></i> <span class="menu-text">Operator</span>
                                <span class="selected"></span>
                            </a>					
                        </li>
<!--                        <li class="has-sub <?php // echo ($this->uri->segment(1) == 'operator' && $this->uri->segment(2) == '' OR $this->uri->segment(2) == 'tambah_operator' OR $this->uri->segment(2) == 'edit_operator') ? 'active' : '';   ?>">
                            <a href="javascript:;" class="">
                                <i class="fa fa-user-md fa-fw"></i> <span class="menu-text">Data Operator dan Pengawas</span>
                                <span class="arrow <?php // echo ($this->uri->segment(1) == 'operator' && $this->uri->segment(2) == '' OR $this->uri->segment(2) == 'tambah_operator' OR $this->uri->segment(2) == 'edit_operator') ? 'open selected' : '';   ?>"></span>
                            </a>
                            <ul class="sub">
                                <li class="<?php // echo ($this->uri->segment(1) == 'operator' && $this->uri->segment(2) == '') ? 'active' : '';   ?>"><a class="" href="<?php // echo site_url('operator')   ?>"><span class="sub-menu-text">List Operator dan Pengawas</span></a></li>
                                <li class="<?php // echo ($this->uri->segment(1) == 'operator' && $this->uri->segment(2) == 'tambah_operator') ? 'active' : '';   ?>"><a class="" href="<?php // echo site_url('operator/tambah_operator');   ?>"><span class="sub-menu-text">Tambah Operator dan Pengawas</span></a></li>
                            </ul>
                        </li>-->
                        <li class="has-sub <?php echo ($this->uri->segment(1) == 'klien' && $this->uri->segment(2) == '' OR $this->uri->segment(2) == 'tambah_klien' OR $this->uri->segment(2) == 'edit_klien') ? 'active' : ''; ?>">
                            <a href="javascript:;" class="">
                                <i class="fa fa-user fa-fw"></i> <span class="menu-text">Data Klien</span>
                                <span class="arrow <?php echo ($this->uri->segment(1) == 'klien' && $this->uri->segment(2) == '' OR $this->uri->segment(2) == 'tambah_klien' OR $this->uri->segment(2) == 'edit_klien') ? 'open selected' : ''; ?>"></span>
                            </a>
                            <ul class="sub">
                                <li class="<?php echo ($this->uri->segment(1) == 'klien' && $this->uri->segment(2) == '') ? 'active' : ''; ?>"><a class="" href="<?php echo site_url('klien'); ?>"><span class="sub-menu-text">List Klien</span></a></li>
                                <!--<li class="<?php // echo ($this->uri->segment(1) == 'klien' && $this->uri->segment(2) == 'tambah_klien') ? 'active' : ''; ?>"><a class="" href="<?php // echo site_url('klien/tambah_klien'); ?>"><span class="sub-menu-text">Tambah Klien</span></a></li>-->
                                <li class="has-sub-sub">
                                    <a href="javascript:;" class=""><span class="sub-menu-text">Data Checklist</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-sub">
                                        <li><a class="" href="#"><span class="sub-sub-menu-text">List Data</span></a></li>
                                        <li><a class="" href="#"><span class="sub-sub-menu-text">Tambah Data</span></a></li>
                                        <li><a class="" href="#"><span class="sub-sub-menu-text">Edit Data</span></a></li>
                                        <li><a class="" href="#"><span class="sub-sub-menu-text">Hapus Data</span></a></li>
                                    </ul>
                                </li>
                        </li>
                    </ul>
                    </li>							
                    <li class="has-sub <?php echo ($this->uri->segment(1) == 'jadwal' && $this->uri->segment(2) == '') ? 'active' : ''; ?>">
                        <a href="javascript:;" class="">
                            <i class="fa fa-calendar fa-fw"></i> <span class="menu-text">Jadwal</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub">
                            <li class="<?php echo ($this->uri->segment(1) == 'jadwal' && $this->uri->segment(2) == '') ? 'active' : ''; ?>"><a class="" href="<?php echo site_url('jadwal') ?>"><span class="sub-menu-text">List Jadwal</span></a></li>
                            <li><a class="" href=""><span class="sub-menu-text">Tambah Jadwal</span></a></li>
                            <li><a class="" href=""><span class="sub-menu-text">Ubah Jadwal</span></a></li>
                            <li><a class="" href=""><span class="sub-menu-text">Hapus Jadwal</span></a></li>
                            <li><a class="" href=""><span class="sub-menu-text">Cetak QR Code</span></a></li>
                        </ul>
                    </li>							
                    <li class="has-sub">
                        <a href="javascript:;" class="">
                            <i class="fa fa-file-o fa-fw"></i> <span class="menu-text">Laporan Pekerjaan</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub">
                            <li><a class="" href="elements.html"><span class="sub-menu-text">Laporan Keseluruhan</span></a></li>
                            <li><a class="" href="notifications.html"><span class="sub-menu-text">Laporan Berdasarkan Operator dan Pengawas</span></a></li>
                            <li><a class="" href="buttons_icons.html"><span class="sub-menu-text">Laporan Berdasarkan Klien</span></a></li>
                        </ul>
                    </li>
                    </ul>
                    <!-- /SIDEBAR MENU -->
                </div>
            </div>
            <!-- /SIDEBAR -->
            <div id="main-content">
                <!-- SAMPLE BOX CONFIGURATION MODAL FORM-->
                <div class="modal fade" id="box-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Box Settings</h4>
                            </div>
                            <div class="modal-body">
                                Here goes box setting content.
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /SAMPLE BOX CONFIGURATION MODAL FORM-->
                <div class="container">
                    <div class="row">
                        <div id="content" class="col-lg-12">
                            <!-- PAGE HEADER-->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="page-header">
                                        <!-- STYLER -->

                                        <!-- /STYLER -->
                                        <!-- BREADCRUMBS -->
                                        <ul class="breadcrumb">
                                            <li>
                                                <i class="fa fa-home"></i>
                                                <a href="<?php echo site_url(); ?>">Home</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo $this->uri->segment(1); ?>">
                                                    <?php
                                                    if ($this->uri->segment(1) == '') {
                                                        echo 'Dashboard';
                                                    }
                                                    else
                                                        echo ucfirst($this->uri->segment(1));
                                                    ?></a>
                                            </li>

                                            <li>
                                                <?php
                                                if ($this->uri->segment(2) != '') {
                                                    echo $this->uri->segment(2);
                                                }
                                                ?>
                                            </li>
                                        </ul>
                                        <!-- /BREADCRUMBS -->
                                        <div class="clearfix">
                                            <h3 class="content-title pull-left">
                                                <?php
                                                if ($this->uri->segment(1) == '') {
                                                    echo 'Dashboard';
                                                }
                                                else
                                                    echo ucfirst($this->uri->segment(1));
                                                ?>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /PAGE HEADER -->
                            <?php echo $this->load->view($konten) ?>
                        </div><!-- /CONTENT-->
                    </div>
                </div>
            </div>
        </section>
        <!--/PAGE -->

    </body>
</html>
