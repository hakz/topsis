<!DOCTYPE html>
<?php
	if($this->session->userdata('is_login') != true){
		redirect(base_url('login'));
	}
?>
<html lang="en">
<head>
	<?=$this->load->view('include/script');?>
</head>
<body>
	<?=$this->load->view('include/navbar');?>
	<div class="container-fluid" id="main-container">
		<?=$this->load->view('include/sidebar');?>
		<div id="main-content" class="clearfix">
			<?=(isset($main)) ? $this->load->view($main) : 'please select main view';?>	
		</div><!-- #main-content -->
	</div><!--/.fluid-container#main-container-->
	<a href="#" id="btn-scroll-up" class="btn btn-small btn-inverse">
		<i class="icon-double-angle-up icon-only"></i>
	</a>
</body>