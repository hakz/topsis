-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2015 at 04:18 AM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `topsis`
--
CREATE DATABASE IF NOT EXISTS `topsis` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `topsis`;

-- --------------------------------------------------------

--
-- Table structure for table `bobotkriteria`
--

CREATE TABLE IF NOT EXISTS `bobotkriteria` (
  `idBobotKriteria` int(11) NOT NULL AUTO_INCREMENT,
  `idKriteria` int(11) NOT NULL,
  `idKompetensi` int(11) NOT NULL,
  `Bobot` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idBobotKriteria`,`idKriteria`,`idKompetensi`),
  KEY `fk_BobotKriteria_Kriteria1` (`idKriteria`),
  KEY `fk_BobotKriteria_Kompetensi1` (`idKompetensi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `bobotkriteria`
--

INSERT INTO `bobotkriteria` (`idBobotKriteria`, `idKriteria`, `idKompetensi`, `Bobot`) VALUES
(1, 1, 1, '2'),
(2, 1, 2, '2'),
(3, 1, 3, '5'),
(4, 2, 1, '5'),
(5, 2, 2, '3'),
(6, 2, 3, '1'),
(7, 3, 1, '2'),
(8, 3, 2, '5'),
(9, 3, 3, '1'),
(10, 4, 1, '4'),
(11, 4, 2, '3'),
(12, 4, 3, '4'),
(13, 5, 1, '3'),
(14, 5, 2, '3'),
(15, 5, 3, '5');

-- --------------------------------------------------------

--
-- Table structure for table `hasil_topsis`
--

CREATE TABLE IF NOT EXISTS `hasil_topsis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `d_positif` double DEFAULT NULL,
  `d_negatif` double DEFAULT NULL,
  `v` double DEFAULT NULL,
  `idPeserta` int(11) DEFAULT NULL,
  `idKompetensi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `hasil_topsis`
--

INSERT INTO `hasil_topsis` (`id`, `d_positif`, `d_negatif`, `v`, `idPeserta`, `idKompetensi`) VALUES
(1, 0.73406493521048, 0.71589023979349, 0.49373266990239, 1, 1),
(2, 0.73345600693731, 0.70117498099036, 0.48874936265193, 2, 1),
(3, 0.60702890613561, 0.71795287861702, 0.54185867826935, 3, 1),
(4, 0.85607514477625, 0.56918795428109, 0.39935640981482, 4, 1),
(5, 0.59467826014559, 0.64275577105335, 0.5194262925116, 5, 1),
(6, 0.64280102401156, 0.87653844531314, 0.57692073628729, 6, 1),
(7, 0.61128714737576, 0.79423594110997, 0.56508210189963, 7, 1),
(8, 0.66245611340856, 0.62543519566038, 0.48562731284562, 8, 1),
(9, 0.46991528660115, 0.71130334235981, 0.60217755199602, 9, 1),
(10, 0.49275893080397, 0.97854778043884, 0.66508755309915, 1, 2),
(11, 0.49677702793518, 0.94926632568842, 0.65645772190003, 2, 2),
(12, 0.84235029751201, 0.57108338175033, 0.40403974387279, 3, 2),
(13, 0.78738054165394, 0.85975665314, 0.52197027415652, 4, 2),
(14, 0.74095566172023, 0.61685910187903, 0.45430283895564, 5, 2),
(15, 0.65690451346304, 0.86514856158009, 0.5684089311771, 6, 2),
(16, 0.79680265575587, 0.7190358023734, 0.47434856829057, 7, 2),
(17, 0.94251325960331, 0.50391190521099, 0.34838435991653, 8, 2),
(18, 0.49469660837879, 0.7415176184546, 0.59982938422738, 9, 2),
(19, 0.66517977344065, 1.0874892889496, 0.62047611399412, 1, 3),
(20, 0.47888056242151, 1.3156533321568, 0.73314487741451, 2, 3),
(21, 1.2259957299621, 0.73769883994571, 0.37566882918068, 3, 3),
(22, 1.5347891929977, 0.19319436489418, 0.11180335832007, 4, 3),
(23, 0.45530052438415, 1.20727401285, 0.72614730095555, 5, 3),
(24, 1.2772608736688, 0.58252633383759, 0.31322203501907, 6, 3),
(25, 0.54425298660369, 1.3304857707782, 0.70969129194099, 7, 3),
(26, 0.78396087273948, 0.90744273619556, 0.53650277875835, 8, 3),
(27, 0.62071264179304, 0.99475713869467, 0.61576957409525, 9, 3);

-- --------------------------------------------------------

--
-- Table structure for table `kompetensi`
--

CREATE TABLE IF NOT EXISTS `kompetensi` (
  `idKompetensi` int(11) NOT NULL AUTO_INCREMENT,
  `NamaKompetensi` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idKompetensi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kompetensi`
--

INSERT INTO `kompetensi` (`idKompetensi`, `NamaKompetensi`) VALUES
(1, 'PROGRAMER'),
(2, 'JARINGAN'),
(3, 'COSTUMER SERVICE');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE IF NOT EXISTS `kriteria` (
  `idKriteria` int(11) NOT NULL AUTO_INCREMENT,
  `NamaKriteria` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idKriteria`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`idKriteria`, `NamaKriteria`) VALUES
(1, 'TES KEPRIBADIAN'),
(2, 'TES LOGIKA'),
(3, 'TES KOMPUTER JARINGAN'),
(4, 'TES MATEMATIKA'),
(5, 'TES BAHASA INGGRIS');

-- --------------------------------------------------------

--
-- Table structure for table `min_max`
--

CREATE TABLE IF NOT EXISTS `min_max` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `min` double DEFAULT NULL,
  `max` double DEFAULT NULL,
  `idKriteria` int(11) DEFAULT NULL,
  `idKompetensi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `min_max`
--

INSERT INTO `min_max` (`id`, `min`, `max`, `idKriteria`, `idKompetensi`) VALUES
(1, 0.403502255696, 0.87873824573796, 1, 1),
(2, 0.403502255696, 0.87873824573796, 1, 2),
(3, 1.00875563924, 2.1968456143449, 1, 3),
(4, 1.3136917052135, 1.9806428786295, 2, 1),
(5, 0.78821502312807, 1.1883857271777, 2, 2),
(6, 0.26273834104269, 0.3961285757259, 2, 3),
(7, 0.4825595908263, 0.80754870301544, 3, 1),
(8, 1.2063989770657, 2.0188717575386, 3, 2),
(9, 0.24127979541315, 0.40377435150772, 3, 3),
(10, 1.0655495079835, 1.5585649519758, 4, 1),
(11, 0.79916213098761, 1.1689237139819, 4, 2),
(12, 1.0655495079835, 1.5585649519758, 4, 3),
(13, 0.69008535272868, 1.2076493672752, 5, 1),
(14, 0.69008535272868, 1.2076493672752, 5, 2),
(15, 1.1501422545478, 2.0127489454586, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `idNilai` int(11) NOT NULL AUTO_INCREMENT,
  `idPeserta` int(11) NOT NULL,
  `idKriteria` int(11) NOT NULL,
  `Nilai` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idNilai`,`idPeserta`,`idKriteria`),
  KEY `fk_Nilai_Peserta` (`idPeserta`),
  KEY `fk_Nilai_Kriteria1` (`idKriteria`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`idNilai`, `idPeserta`, `idKriteria`, `Nilai`) VALUES
(1, 1, 1, '70'),
(2, 1, 2, '65'),
(3, 1, 3, '80'),
(4, 1, 4, '87'),
(5, 1, 5, '98'),
(6, 2, 1, '98'),
(7, 2, 2, '65'),
(8, 2, 3, '79'),
(9, 2, 4, '88'),
(10, 2, 5, '77'),
(11, 3, 1, '45'),
(12, 3, 2, '90'),
(13, 3, 3, '55'),
(14, 3, 4, '87'),
(15, 3, 5, '88'),
(16, 4, 1, '45'),
(17, 4, 2, '88'),
(18, 4, 3, '82'),
(19, 4, 4, '70'),
(20, 4, 5, '56'),
(21, 5, 1, '90'),
(22, 5, 2, '78'),
(23, 5, 3, '56'),
(24, 5, 4, '78'),
(25, 5, 5, '87'),
(26, 6, 1, '56'),
(27, 6, 2, '98'),
(28, 6, 3, '76'),
(29, 6, 4, '98'),
(30, 6, 5, '56'),
(31, 7, 1, '90'),
(32, 7, 2, '87'),
(33, 7, 3, '54'),
(34, 7, 4, '67'),
(35, 7, 5, '98'),
(36, 8, 1, '78'),
(37, 8, 2, '78'),
(38, 8, 3, '49'),
(39, 8, 4, '96'),
(40, 8, 5, '68'),
(41, 9, 1, '76'),
(42, 9, 2, '87'),
(43, 9, 3, '68'),
(44, 9, 4, '78'),
(45, 9, 5, '89');

-- --------------------------------------------------------

--
-- Table structure for table `normalisasi`
--

CREATE TABLE IF NOT EXISTS `normalisasi` (
  `idNormalisasi` int(11) NOT NULL AUTO_INCREMENT,
  `nilai_normalisasi` double DEFAULT NULL,
  `idPeserta` int(11) DEFAULT NULL,
  PRIMARY KEY (`idNormalisasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `normalisasi`
--

INSERT INTO `normalisasi` (`idNormalisasi`, `nilai_normalisasi`, `idPeserta`) VALUES
(1, 0.31383508776355, 1),
(2, 0.43936912286898, 2),
(3, 0.201751127848, 3),
(4, 0.201751127848, 4),
(5, 0.403502255696, 5),
(6, 0.25106807021084, 6),
(7, 0.403502255696, 7),
(8, 0.34970195493653, 8),
(9, 0.34073523814329, 9),
(10, 0.26273834104269, 1),
(11, 0.26273834104269, 2),
(12, 0.36379154913603, 3),
(13, 0.35570729248857, 4),
(14, 0.31528600925123, 5),
(15, 0.3961285757259, 6),
(16, 0.35166516416483, 7),
(17, 0.31528600925123, 8),
(18, 0.35166516416483, 9),
(19, 0.3939261965929, 1),
(20, 0.38900211913549, 2),
(21, 0.27082426015762, 3),
(22, 0.40377435150772, 4),
(23, 0.27574833761503, 5),
(24, 0.37422988676325, 6),
(25, 0.26590018270021, 7),
(26, 0.24127979541315, 8),
(27, 0.33483726710396, 9),
(28, 0.34590599699464, 1),
(29, 0.34988192799458, 2),
(30, 0.34590599699464, 3),
(31, 0.27831516999569, 4),
(32, 0.3101226179952, 5),
(33, 0.38964123799396, 6),
(34, 0.26638737699587, 7),
(35, 0.38168937599409, 8),
(36, 0.3101226179952, 9),
(37, 0.40254978909172, 1),
(38, 0.31628912000064, 2),
(39, 0.36147328000073, 3),
(40, 0.23002845090956, 4),
(41, 0.35736562909163, 5),
(42, 0.23002845090956, 6),
(43, 0.40254978909172, 7),
(44, 0.27932026181875, 8),
(45, 0.36558093090983, 9);

-- --------------------------------------------------------

--
-- Table structure for table `normalisasi_terbobot`
--

CREATE TABLE IF NOT EXISTS `normalisasi_terbobot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai` double DEFAULT NULL,
  `idPeserta` int(11) DEFAULT NULL,
  `idKriteria` int(11) DEFAULT NULL,
  `idKompetensi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=136 ;

--
-- Dumping data for table `normalisasi_terbobot`
--

INSERT INTO `normalisasi_terbobot` (`id`, `nilai`, `idPeserta`, `idKriteria`, `idKompetensi`) VALUES
(1, 0.6276701755271, 1, 1, 1),
(2, 0.87873824573796, 2, 1, 1),
(3, 0.403502255696, 3, 1, 1),
(4, 0.403502255696, 4, 1, 1),
(5, 0.807004511392, 5, 1, 1),
(6, 0.50213614042168, 6, 1, 1),
(7, 0.807004511392, 7, 1, 1),
(8, 0.69940390987306, 8, 1, 1),
(9, 0.68147047628658, 9, 1, 1),
(10, 1.3136917052135, 1, 2, 1),
(11, 1.3136917052135, 2, 2, 1),
(12, 1.8189577456802, 3, 2, 1),
(13, 1.7785364624429, 4, 2, 1),
(14, 1.5764300462562, 5, 2, 1),
(15, 1.9806428786295, 6, 2, 1),
(16, 1.7583258208242, 7, 2, 1),
(17, 1.5764300462562, 8, 2, 1),
(18, 1.7583258208242, 9, 2, 1),
(19, 0.7878523931858, 1, 3, 1),
(20, 0.77800423827098, 2, 3, 1),
(21, 0.54164852031524, 3, 3, 1),
(22, 0.80754870301544, 4, 3, 1),
(23, 0.55149667523006, 5, 3, 1),
(24, 0.7484597735265, 6, 3, 1),
(25, 0.53180036540042, 7, 3, 1),
(26, 0.4825595908263, 8, 3, 1),
(27, 0.66967453420792, 9, 3, 1),
(28, 1.3836239879786, 1, 4, 1),
(29, 1.3995277119783, 2, 4, 1),
(30, 1.3836239879786, 3, 4, 1),
(31, 1.1132606799828, 4, 4, 1),
(32, 1.2404904719808, 5, 4, 1),
(33, 1.5585649519758, 6, 4, 1),
(34, 1.0655495079835, 7, 4, 1),
(35, 1.5267575039764, 8, 4, 1),
(36, 1.2404904719808, 9, 4, 1),
(37, 1.2076493672752, 1, 5, 1),
(38, 0.94886736000192, 2, 5, 1),
(39, 1.0844198400022, 3, 5, 1),
(40, 0.69008535272868, 4, 5, 1),
(41, 1.0720968872749, 5, 5, 1),
(42, 0.69008535272868, 6, 5, 1),
(43, 1.2076493672752, 7, 5, 1),
(44, 0.83796078545625, 8, 5, 1),
(45, 1.0967427927295, 9, 5, 1),
(46, 0.6276701755271, 1, 1, 2),
(47, 0.87873824573796, 2, 1, 2),
(48, 0.403502255696, 3, 1, 2),
(49, 0.403502255696, 4, 1, 2),
(50, 0.807004511392, 5, 1, 2),
(51, 0.50213614042168, 6, 1, 2),
(52, 0.807004511392, 7, 1, 2),
(53, 0.69940390987306, 8, 1, 2),
(54, 0.68147047628658, 9, 1, 2),
(55, 0.78821502312807, 1, 2, 2),
(56, 0.78821502312807, 2, 2, 2),
(57, 1.0913746474081, 3, 2, 2),
(58, 1.0671218774657, 4, 2, 2),
(59, 0.94585802775369, 5, 2, 2),
(60, 1.1883857271777, 6, 2, 2),
(61, 1.0549954924945, 7, 2, 2),
(62, 0.94585802775369, 8, 2, 2),
(63, 1.0549954924945, 9, 2, 2),
(64, 1.9696309829645, 1, 3, 2),
(65, 1.9450105956774, 2, 3, 2),
(66, 1.3541213007881, 3, 3, 2),
(67, 2.0188717575386, 4, 3, 2),
(68, 1.3787416880751, 5, 3, 2),
(69, 1.8711494338162, 6, 3, 2),
(70, 1.329500913501, 7, 3, 2),
(71, 1.2063989770657, 8, 3, 2),
(72, 1.6741863355198, 9, 3, 2),
(73, 1.0377179909839, 1, 4, 2),
(74, 1.0496457839837, 2, 4, 2),
(75, 1.0377179909839, 3, 4, 2),
(76, 0.83494550998707, 4, 4, 2),
(77, 0.9303678539856, 5, 4, 2),
(78, 1.1689237139819, 6, 4, 2),
(79, 0.79916213098761, 7, 4, 2),
(80, 1.1450681279823, 8, 4, 2),
(81, 0.9303678539856, 9, 4, 2),
(82, 1.2076493672752, 1, 5, 2),
(83, 0.94886736000192, 2, 5, 2),
(84, 1.0844198400022, 3, 5, 2),
(85, 0.69008535272868, 4, 5, 2),
(86, 1.0720968872749, 5, 5, 2),
(87, 0.69008535272868, 6, 5, 2),
(88, 1.2076493672752, 7, 5, 2),
(89, 0.83796078545625, 8, 5, 2),
(90, 1.0967427927295, 9, 5, 2),
(91, 1.5691754388178, 1, 1, 3),
(92, 2.1968456143449, 2, 1, 3),
(93, 1.00875563924, 3, 1, 3),
(94, 1.00875563924, 4, 1, 3),
(95, 2.01751127848, 5, 1, 3),
(96, 1.2553403510542, 6, 1, 3),
(97, 2.01751127848, 7, 1, 3),
(98, 1.7485097746826, 8, 1, 3),
(99, 1.7036761907164, 9, 1, 3),
(100, 0.26273834104269, 1, 2, 3),
(101, 0.26273834104269, 2, 2, 3),
(102, 0.36379154913603, 3, 2, 3),
(103, 0.35570729248857, 4, 2, 3),
(104, 0.31528600925123, 5, 2, 3),
(105, 0.3961285757259, 6, 2, 3),
(106, 0.35166516416483, 7, 2, 3),
(107, 0.31528600925123, 8, 2, 3),
(108, 0.35166516416483, 9, 2, 3),
(109, 0.3939261965929, 1, 3, 3),
(110, 0.38900211913549, 2, 3, 3),
(111, 0.27082426015762, 3, 3, 3),
(112, 0.40377435150772, 4, 3, 3),
(113, 0.27574833761503, 5, 3, 3),
(114, 0.37422988676325, 6, 3, 3),
(115, 0.26590018270021, 7, 3, 3),
(116, 0.24127979541315, 8, 3, 3),
(117, 0.33483726710396, 9, 3, 3),
(118, 1.3836239879786, 1, 4, 3),
(119, 1.3995277119783, 2, 4, 3),
(120, 1.3836239879786, 3, 4, 3),
(121, 1.1132606799828, 4, 4, 3),
(122, 1.2404904719808, 5, 4, 3),
(123, 1.5585649519758, 6, 4, 3),
(124, 1.0655495079835, 7, 4, 3),
(125, 1.5267575039764, 8, 4, 3),
(126, 1.2404904719808, 9, 4, 3),
(127, 2.0127489454586, 1, 5, 3),
(128, 1.5814456000032, 2, 5, 3),
(129, 1.8073664000036, 3, 5, 3),
(130, 1.1501422545478, 4, 5, 3),
(131, 1.7868281454582, 5, 5, 3),
(132, 1.1501422545478, 6, 5, 3),
(133, 2.0127489454586, 7, 5, 3),
(134, 1.3966013090938, 8, 5, 3),
(135, 1.8279046545492, 9, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `peserta`
--

CREATE TABLE IF NOT EXISTS `peserta` (
  `idPeserta` int(11) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idPeserta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `peserta`
--

INSERT INTO `peserta` (`idPeserta`, `Nama`) VALUES
(1, 'UYOK'),
(2, 'HAFIZ'),
(3, 'WAHYU'),
(4, 'FERIAN'),
(5, 'ANA'),
(6, 'ZITA'),
(7, 'YOGA'),
(8, 'HENDRA'),
(9, 'YUSHA');

-- --------------------------------------------------------

--
-- Table structure for table `v_normalisasi`
--

CREATE TABLE IF NOT EXISTS `v_normalisasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vx` double DEFAULT NULL,
  `idPeserta` int(11) DEFAULT NULL,
  `idKompetensi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `v_normalisasi`
--

INSERT INTO `v_normalisasi` (`id`, `vx`, `idPeserta`, `idKompetensi`) VALUES
(1, 0.31503875697656, 1, 1),
(2, 0.31185903034002, 2, 1),
(3, 0.34574678741166, 3, 1),
(4, 0.254819570606, 4, 1),
(5, 0.33143322998283, 5, 1),
(6, 0.36811903022314, 6, 1),
(7, 0.36056508678543, 7, 1),
(8, 0.30986692661637, 8, 1),
(9, 0.38423478741546, 9, 1),
(10, 0.41693300802906, 1, 2),
(11, 0.41152310152297, 2, 2),
(12, 0.25328621020075, 3, 2),
(13, 0.32721501927339, 4, 2),
(14, 0.2847953601286, 5, 2),
(15, 0.35632668866218, 6, 2),
(16, 0.29736171502549, 7, 2),
(17, 0.21839671852741, 8, 2),
(18, 0.37602368034823, 9, 2),
(19, 0.36611605596901, 1, 3),
(20, 0.4325970088438, 2, 3),
(21, 0.22166589009328, 3, 3),
(22, 0.065970314842163, 4, 3),
(23, 0.42846804233451, 5, 3),
(24, 0.18481874405379, 6, 3),
(25, 0.41875806481641, 7, 3),
(26, 0.3165670312609, 8, 3),
(27, 0.36333893081274, 9, 3);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bobotkriteria`
--
ALTER TABLE `bobotkriteria`
  ADD CONSTRAINT `fk_BobotKriteria_Kompetensi1` FOREIGN KEY (`idKompetensi`) REFERENCES `kompetensi` (`idKompetensi`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_BobotKriteria_Kriteria1` FOREIGN KEY (`idKriteria`) REFERENCES `kriteria` (`idKriteria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nilai`
--
ALTER TABLE `nilai`
  ADD CONSTRAINT `fk_Nilai_Kriteria1` FOREIGN KEY (`idKriteria`) REFERENCES `kriteria` (`idKriteria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Nilai_Peserta` FOREIGN KEY (`idPeserta`) REFERENCES `peserta` (`idPeserta`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
