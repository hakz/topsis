SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `topsis` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `topsis` ;

-- -----------------------------------------------------
-- Table `topsis`.`Peserta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `topsis`.`Peserta` (
  `idPeserta` INT NOT NULL AUTO_INCREMENT ,
  `Nama` VARCHAR(200) NULL ,
  PRIMARY KEY (`idPeserta`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `topsis`.`Kriteria`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `topsis`.`Kriteria` (
  `idKriteria` INT NOT NULL AUTO_INCREMENT ,
  `NamaKriteria` VARCHAR(45) NULL ,
  PRIMARY KEY (`idKriteria`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `topsis`.`Nilai`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `topsis`.`Nilai` (
  `idNilai` INT NOT NULL AUTO_INCREMENT ,
  `idPeserta` INT NOT NULL ,
  `idKriteria` INT NOT NULL ,
  `Nilai` VARCHAR(45) NULL ,
  PRIMARY KEY (`idNilai`, `idPeserta`, `idKriteria`) ,
  INDEX `fk_Nilai_Peserta` (`idPeserta` ASC) ,
  INDEX `fk_Nilai_Kriteria1` (`idKriteria` ASC) ,
  CONSTRAINT `fk_Nilai_Peserta`
    FOREIGN KEY (`idPeserta` )
    REFERENCES `topsis`.`Peserta` (`idPeserta` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Nilai_Kriteria1`
    FOREIGN KEY (`idKriteria` )
    REFERENCES `topsis`.`Kriteria` (`idKriteria` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `topsis`.`Kompetensi`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `topsis`.`Kompetensi` (
  `idKompetensi` INT NOT NULL AUTO_INCREMENT ,
  `NamaKompetensi` VARCHAR(45) NULL ,
  PRIMARY KEY (`idKompetensi`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `topsis`.`BobotKriteria`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `topsis`.`BobotKriteria` (
  `idBobotKriteria` INT NOT NULL AUTO_INCREMENT ,
  `idKriteria` INT NOT NULL ,
  `idKompetensi` INT NOT NULL ,
  `Bobot` VARCHAR(45) NULL ,
  PRIMARY KEY (`idBobotKriteria`, `idKriteria`, `idKompetensi`) ,
  INDEX `fk_BobotKriteria_Kriteria1` (`idKriteria` ASC) ,
  INDEX `fk_BobotKriteria_Kompetensi1` (`idKompetensi` ASC) ,
  CONSTRAINT `fk_BobotKriteria_Kriteria1`
    FOREIGN KEY (`idKriteria` )
    REFERENCES `topsis`.`Kriteria` (`idKriteria` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_BobotKriteria_Kompetensi1`
    FOREIGN KEY (`idKompetensi` )
    REFERENCES `topsis`.`Kompetensi` (`idKompetensi` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
